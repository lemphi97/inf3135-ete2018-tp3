# Based on `Makefile` from:
# https://gitlab.com/ablondin/inf3135-ete2018-tp2

SRC_DIR = src
BIN_DIR = bin
BATS_FILE = test.bats
EXEC = tp3
TEST_EXEC = $(patsubst %.c,%,$(wildcard $(SRC_DIR)/test*.c))

.PHONY: exec bindir clean source test testbats testbin testcunit

exec: source bindir
	cp $(SRC_DIR)/tp3 $(BIN_DIR)
	cp $(SRC_DIR)/countries.json `pwd` 

bindir:
	mkdir -p $(BIN_DIR)

clean:
	make clean -C $(SRC_DIR)
	rm -rf $(BIN_DIR) countries.json *.text *.dot *.png
	
source:
	make -C $(SRC_DIR)

test: exec testbin testcunit testbats

testbats:
	bats $(BATS_FILE)

testbin: source bindir
	$(MAKE) test -C $(SRC_DIR)
	cp $(TEST_EXEC) $(BIN_DIR)

testcunit:
	for test in `ls $(BIN_DIR)/test*` ; do \
		$$test; \
	done
