#!/usr/bin/env bats

@test "Default program working, in default format" {
  run bin/tp3
  [ "$status" -eq 0 ]
  [ "${lines[0]}" = "Name: Aruba" ]
  [ "${lines[1]}" = "Code: ABW" ]
  [ "${lines[498]}" = "Name: Zimbabwe" ]
  [ "${lines[499]}" = "Code: ZWE" ]
}

@test "Default program working, in default format and with filename" {
  run bin/tp3 --output-filename test_file.text
  [ "$status" -eq 0 ]
  run cat test_file.text
  [ "${lines[0]}" = "Name: Aruba" ]
  [ "${lines[1]}" = "Code: ABW" ]
  [ "${lines[498]}" = "Name: Zimbabwe" ]
  [ "${lines[499]}" = "Code: ZWE" ]
  run rm test_file.text
}

@test "Option show-capital, in default format" {
  run bin/tp3 --show-capital
  [ "$status" -eq 0 ]
  [ "${lines[0]}" = "Name: Aruba" ]
  [ "${lines[1]}" = "Code: ABW" ]
  [ "${lines[2]}" = "Capital: Oranjestad" ]
  [ "${lines[747]}" = "Name: Zimbabwe" ]
  [ "${lines[748]}" = "Code: ZWE" ]
  [ "${lines[749]}" = "Capital: Harare" ]
}

@test "Option show-capital, in default format and with filename" {
  run bin/tp3 --show-capital --output-filename test_file.text
  [ "$status" -eq 0 ]
  run cat test_file.text
  [ "${lines[0]}" = "Name: Aruba" ]
  [ "${lines[1]}" = "Code: ABW" ]
  [ "${lines[2]}" = "Capital: Oranjestad" ]
  [ "${lines[747]}" = "Name: Zimbabwe" ]
  [ "${lines[748]}" = "Code: ZWE" ]
  [ "${lines[749]}" = "Capital: Harare" ] 
  run rm test_file.text
}

@test "Option show-languages, in default format" {
  run bin/tp3 --show-languages
  [ "$status" -eq 0 ]
  [ "${lines[0]}" = "Name: Aruba" ]
  [ "${lines[1]}" = "Code: ABW" ]
  [ "${lines[2]}" = "Languages: Dutch, Papiamento" ]
  [ "${lines[747]}" = "Name: Zimbabwe" ]
  [ "${lines[748]}" = "Code: ZWE" ]
  [ "${lines[749]}" = "Languages: Chibarwe, English, Kalanga, Khoisan, Ndau, Northern Ndebele, Chewa, Shona, Sotho, Tonga, Tswana, Tsonga, Venda, Xhosa, Zimbabwean Sign Language" ]
}

@test "Option show-languages, in default format and with filename" {
  run bin/tp3 --show-languages --output-filename test_file.text
  [ "$status" -eq 0 ]
  run cat test_file.text
  [ "${lines[0]}" = "Name: Aruba" ]
  [ "${lines[1]}" = "Code: ABW" ]
  [ "${lines[2]}" = "Languages: Dutch, Papiamento" ]
  [ "${lines[747]}" = "Name: Zimbabwe" ]
  [ "${lines[748]}" = "Code: ZWE" ]
  [ "${lines[749]}" = "Languages: Chibarwe, English, Kalanga, Khoisan, Ndau, Northern Ndebele, Chewa, Shona, Sotho, Tonga, Tswana, Tsonga, Venda, Xhosa, Zimbabwean Sign Language" ]
  run rm test_file.text
}

@test "Option show-borders, in default format" {
  run bin/tp3 --show-borders
  [ "$status" -eq 0 ]
  [ "${lines[0]}" = "Name: Aruba" ]
  [ "${lines[1]}" = "Code: ABW" ]
  [ "${lines[2]}" = "Borders:" ]
  [ "${lines[747]}" = "Name: Zimbabwe" ]
  [ "${lines[748]}" = "Code: ZWE" ]
  [ "${lines[749]}" = "Borders: BWA, MOZ, ZAF, ZMB" ]
}

@test "Option show-borders, in default format and with filename" {
  run bin/tp3 --show-borders --output-filename test_file.text
  [ "$status" -eq 0 ]
  run cat test_file.text
  [ "${lines[0]}" = "Name: Aruba" ]
  [ "${lines[1]}" = "Code: ABW" ]
  [ "${lines[2]}" = "Borders:" ]
  [ "${lines[747]}" = "Name: Zimbabwe" ]
  [ "${lines[748]}" = "Code: ZWE" ]
  [ "${lines[749]}" = "Borders: BWA, MOZ, ZAF, ZMB" ]
  run rm test_file.text
}

@test "Wrong country code" {
  run bin/tp3 --country error
  [ "$status" -eq 5 ]
  [ "${lines[0]}" = "Error: couldn't find country or region". ]
  [ "${lines[1]}" = "Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]" ]
}

@test "Wrong region name" {
  run bin/tp3 --region Asia
  [ "$status" -eq 5 ]
  [ "${lines[0]}" = "Error: couldn't find country or region." ]
  [ "${lines[1]}" = "Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]" ]
}

@test "Option country working in default format" {
  run bin/tp3 --show-borders --show-languages --show-capital --country can
  [ "$status" -eq 0 ]
  [ "${lines[0]}" = "Name: Canada" ]
  [ "${lines[4]}" = "Borders: USA" ]
}

@test "Option country working, in default format and filename" {
  run bin/tp3 --show-borders --show-languages --show-capital --country can --output-filename test_file.text
  [ "$status" -eq 0 ]
  run cat test_file.text
  [ "${lines[0]}" = "Name: Canada" ]
  [ "${lines[4]}" = "Borders: USA" ]
  run rm test_file.text
}

@test "Option region working in default format" {
  run bin/tp3 --show-borders --show-languages --show-capital --region asia
  [ "$status" -eq 0 ]
  [ "${lines[0]}" = "Name: Afghanistan" ]
  [ "${lines[4]}" = "Borders: IRN, PAK, TKM, UZB, TJK, CHN" ]
  [ "${lines[245]}" = "Name: Yemen" ]
  [ "${lines[249]}" = "Borders: OMN, SAU" ]
}

@test "Option region working, in default format and filename" {
  run bin/tp3 --show-borders --show-languages --show-capital --region asia --output-filename test_file.text
  [ "$status" -eq 0 ]
  run cat test_file.text
  [ "${lines[0]}" = "Name: Afghanistan" ]
  [ "${lines[4]}" = "Borders: IRN, PAK, TKM, UZB, TJK, CHN" ]
  [ "${lines[245]}" = "Name: Yemen" ]
  [ "${lines[249]}" = "Borders: OMN, SAU" ]
  run rm test_file.text
}

@test "Option help" {
  run bin/tp3 --show-borders --show-languages --show-capital --help
  [ "$status" -eq 0 ]
  [ "${lines[0]}" = "Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]" ]
}

@test "Wrong output-format" {
  run bin/tp3 --output-format error
  [ "$status" -eq 6 ]
  [ "${lines[0]}" = "Error: only support format png, dot, text." ]
  [ "${lines[1]}" = "Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]" ]
}

@test "Option output-format with text" {
  run bin/tp3 --output-format text
  [ "$status" -eq 0 ]
  [ "${lines[0]}" = "Name: Aruba" ]
  [ "${lines[1]}" = "Code: ABW" ]
  [ "${lines[498]}" = "Name: Zimbabwe" ]
  [ "${lines[499]}" = "Code: ZWE" ]
}

@test "Use country with region" {
  run bin/tp3 --country can --region americas
  [ "$status" -eq 1 ]
  [ "${lines[0]}" = "Error: can't use the options \`--country COUNTRY\` with \`--region REGION\`." ] 
  [ "${lines[1]}" = "Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]" ]
}

@test "Use show-flag with text format" {
  run bin/tp3 --show-flag --output-format text
  [ "$status" -eq 3 ]
  [ "${lines[0]}" = "Error: can't use text format with \`--show-flag\`." ]
  [ "${lines[1]}" = "Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]" ]
  run bin/tp3 --show-flag 
  [ "$status" -eq 3 ]
  [ "${lines[0]}" = "Error: can't use text format with \`--show-flag\`." ]
  [ "${lines[1]}" = "Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]" ]
}

@test "Too many arguments used" {
  run bin/tp3 --country can usa
  [ "$status" -eq 2 ]
  [ "${lines[0]}" = "Error: too many arguments." ]
  [ "${lines[1]}" = "Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]" ]
}

#Test not suported by `.gitlab-ci.yml`, so it won't be used. Works otherwise.

#@test "Option output-format working with dot format" {
#  run bin/tp3 --show-borders --show-languages --show-capital --show-flag --output-format dot
#  [ "$status" -eq 0 ]
#  run cat test_file.dot
#  [ "${lines[0]}" = "graph {" ]
#  [ "${lines[1]}" = "abw [" ]
#  [ "${lines[2]}" = "    shape = none," ]
#  [ "${lines[3]}" = "    label = <<table border=\"0\" cellspacing=\"0\">" ]
#  [ "${lines[4]}" = "         <tr><td align=\"center\" border=\"1\" fixedsize=\"true\" width=\"200\" height=\"100\"><img src=\"`pwd`/image/abw.png\" scale=\"true\"/></td></tr>" ]
#  [ "${lines[5]}" = "         <tr><td align=\"left\" border=\"1\"><b>Name</b>:Aruba</td></tr>" ]
#  [ "${lines[6]}" = "         <tr><td align=\"left\" border=\"1\"><b>Code</b>: ABW</td></tr>" ]
#  [ "${lines[7]}" = "         <tr><td align=\"left\" border=\"1\"><b>Capital</b>:Oranjestad </td></tr>" ]
#  [ "${lines[8]}" = "         <tr><td align=\"left\" border=\"1\"><b>Language</b>: Dutch,Papiamento</td></tr>" ]
#  [ "${lines[9]}" = "         <tr><td align=\"left\" border=\"1\"><b>Borders</b>: </td></tr>" ]
#  [ "${lines[10]}" = "    </table>>" ]
#  [ "${lines[11]}" = "   ];" ]
#  [ "${lines[2751]}" = "    afg--chn" ]
#  [ "${lines[3075]}" = "    zmb--zwe" ]
#  [ "${lines[3076]}" = "    }" ]
#}  

@test "Option output-format working with dot format and filename" {
  run bin/tp3 --show-borders --show-languages --show-capital --country abw --output-filename test_file.dot --output-format dot --show-flag
  [ "$status" -eq 0 ]
  run cat test_file.dot
  [ "${lines[0]}" = "graph {" ]
  [ "${lines[1]}" = "abw [" ]
  [ "${lines[2]}" = "    shape = none," ]
  [ "${lines[3]}" = "    label = <<table border=\"0\" cellspacing=\"0\">" ]
  [ "${lines[4]}" = "         <tr><td align=\"center\" border=\"1\" fixedsize=\"true\" width=\"200\" height=\"100\"><img src=\"`pwd`/image/abw.png\" scale=\"true\"/></td></tr>" ]
  [ "${lines[5]}" = "         <tr><td align=\"left\" border=\"1\"><b>Name</b>:Aruba</td></tr>" ]
  [ "${lines[6]}" = "         <tr><td align=\"left\" border=\"1\"><b>Code</b>: ABW</td></tr>" ]
  [ "${lines[7]}" = "         <tr><td align=\"left\" border=\"1\"><b>Capital</b>:Oranjestad </td></tr>" ]
  [ "${lines[8]}" = "         <tr><td align=\"left\" border=\"1\"><b>Language</b>: Dutch,Papiamento</td></tr>" ]
  [ "${lines[9]}" = "         <tr><td align=\"left\" border=\"1\"><b>Borders</b>: </td></tr>" ]
  [ "${lines[10]}" = "    </table>>" ]
  [ "${lines[11]}" = "   ];" ]
  [ "${lines[12]}" = "}" ]
  run rm test_file.dot
}

@test "Option output-format working with png format and filename" {
  run bin/tp3 --country abw --output-filename test_file.dot --output-format png
  #[ "$status" -eq 0 ]   //Can't be tested for program since this gives the status of the Graphviz operation, and not `tp3`.
  run cat test_file.dot
  [ "${lines[0]}" = "graph {" ]
  [ "${lines[1]}" = "abw [" ]
  [ "${lines[2]}" = "    shape = none," ]
  [ "${lines[3]}" = "    label = <<table border=\"0\" cellspacing=\"0\">" ]
  [ "${lines[4]}" = "         <tr><td align=\"left\" border=\"1\"><b>Name</b>:Aruba</td></tr>" ]
  [ "${lines[5]}" = "         <tr><td align=\"left\" border=\"1\"><b>Code</b>: ABW</td></tr>" ]
  [ "${lines[6]}" = "    </table>>" ]
  [ "${lines[7]}" = "   ];" ]  
  [ "${lines[8]}" = "}" ]
  #run ls test_file.png -s
  #[ "${lines[0]}" != "0 test_file.png" ]   //Not compatible with `.gitlab-ci.yml` since it doesn't install Graphviz and the file will be empty.
  #[ "$status" -eq 0 ]
  run rm test_file.dot test_file.png

  run bin/tp3 --country abw --output-format png --output-filename test_file.dot
  #[ "$status" -eq 0 ]   //Can't be tested for program since this gives the status of the Graphviz operation, and not `tp3`.
  run cat test_file.dot
  [ "${lines[0]}" = "graph {" ]
  [ "${lines[1]}" = "abw [" ]
  [ "${lines[2]}" = "    shape = none," ]
  [ "${lines[3]}" = "    label = <<table border=\"0\" cellspacing=\"0\">" ]
  [ "${lines[4]}" = "         <tr><td align=\"left\" border=\"1\"><b>Name</b>:Aruba</td></tr>" ]
  [ "${lines[5]}" = "         <tr><td align=\"left\" border=\"1\"><b>Code</b>: ABW</td></tr>" ]
  [ "${lines[6]}" = "    </table>>" ]
  [ "${lines[7]}" = "   ];" ]
  [ "${lines[8]}" = "}" ]
  #run ls test_file.png -s
  #[ "${lines[0]}" != "0 test_file.png" ]   //Not compatible with `.gitlab-ci.yml` since it doesn't install Graphviz and the file will be empty.
  #[ "$status" -eq 0 ]
  run rm test_file.dot test_file.png
}

@test "Wrong use of output-format png" { 
  run bin/tp3 --output-format png
  [ "$status" -eq 7 ]
  [ "${lines[0]}" = "Error: can't use png format without filename for output." ]
}

