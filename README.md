# Travail pratique 3

## Description

le projet du travail pratique 3 consiste a créer un programme qui charge des données géographiques sous format JSON
provenant du projet [countries](https://github.com/mledoze/countries)  et qui effectue certains traitements de ces
données.

Ce projet s'inscrit dans le cadre de la préparation au diplôme de baccalauréat.
Cours : construction et maintenance de logiciels INF3135
Session : été 2018
Université de Québec a Montréal  


## Auteurs

- Youssef benkirane (BENY28097903)
- Philippe Lemieux (LEMP19129706)

## Fonctionnement

le programme permet d'afficher des informations sur différents pays.

Plus précisément, par exemple les informations suivantes:

- Les langues qui y sont parlées (`--show-languages`);
- La capitale (`--show-capital`);
- Les pays frontaliers (`--show-borders`);
- Le drapeau (`--show-flag`).
Par défaut, tous les pays sont traités, mais il est possible de restreindre
l'affichage:

1) À l'aide de l'argument `--country COUNTRY`, on peut afficher un seul pays,
par exemple:

```sh
$ bin/tp3 --country can --show-languages --show-capital --show-borders
Name: Canada               
Code: CAN                  
Capital: Ottawa            
Languages: English, French 
Borders: USA               
```

2) À l'aide de l'argument `--region REGION`, on peut afficher seulement les
pays d'une région (continent), par exemple:

```sh
$ bin/tp3 --region oceania --show-languages --show-capital --show-borders
Name: American Samoa      
Code: ASM                 
Capital: Pago Pago        
Languages: English, Samoan
Borders:                  
Name: Australia    
Code: AUS          
Capital: Canberra  
Languages: English 
Borders:           
...
Name: Samoa               
Code: WSM                 
Capital: Apia             
Languages: English, Samoan
Borders:                  
```

Pour utiliser l'aide du programme, on saisie la commande suivante:

```
$ bin/tp3 --help
```

Ce message sera affiché:

```sh
Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]
  [--show-languages] [--show-capital] [--show-borders] [--show-flag]
  [--country COUNTRY] [--region REGION]

 Displays information about countries.
 Optional arguments:

 --help                       Show this help message and exit.
 --output-format FORMAT       Selects the ouput format (either "text", "dot" or "png").
                              The "dot" format is the one recognized by Graphviz.
                              the default format is "text".
 --output-filename FILENAME   The name of the output filename. This argument is
                              mandatory for the "png" format. For the "text" and "dot"
                              format, the result is printed on stdout 
                              if no output filename is given.
 --show-languages             The official languages of each country are displayed.
 --show-capital               The capital of each country is displayed.
 --show-borders               The borders of each country are displayed.
 --show-flag                  The flag of each country is displayed
                              (only for "dot" and "png" format).
 --country COUNTRY            The country code (e.g. "can", "usa") to be displayed.
                              Cannot be used with `--region REGION`.
 --region REGION              The region of the countries to be displayed.
                              The supported regions are "africa", "americas",
                              "asia", "europe" and "oceania".
                              Cannot be used with `--country COUNTRY`.

```

le programme supporte trois formats de sorties :

 1) Le format texte (`--output-format text`), qui affiche sous forme textuelle
 les informations demandées (l'option `--show-flag` est ignorée dans ce cas). 
 Par exemple:

 ```sh
 $ bin/tp3 --country can
 Name: Canada               
 Code: CAN                  
 ```

 2) Le format Graphviz (`--output-format dot`), qui affiche sur la sortie
 standard un fichier respectant le format `dot` de Graphviz. Par exemple:


 ```sh
 $ bin/tp3 --country can --show-languages --show-capital --show-borders --show-flag --output-format dot
 graph {
     can [
             shape = none,
             label = <<table border="0" cellspacing="0">
             <tr><td align="center" border="1" fixedsize="true" width="200" height="100">
               <img src="can.png" scale="true"/>
             </td></tr> 
             <tr><td align="left" border="1"><b>Name</b>:Canada</td></tr>
             <tr><td align="left" border="1"><b>Code</b>:CAN</td></tr>
             <tr><td align="left" border="1"><b>Capital</b>:Ottawa</td></tr>
             <tr><td align="left" border="1"><b>Language</b>:French,English</td></tr>
             <tr><td align="left" border="1"><b>Borders</b>:USA</td></tr>                                                                             </table>>
        ];
}
```

Ensuite, il est possible de passer ce fichier à Graphviz pour en faire un rendu
à l'aide de la commande

```sh
$ bin/tp3 --country can --show-languages --show-capital --show-borders --show-flag --output-format dot --output-filename
canada.dot
$ neato -Goverlap=false -Tpng -o canada.png canada.dot
```

et on obtient l'image suivante:

![](image/canada.png)

3) Le format PNG (`--output-format png`), produit une image au format PNG
baser sur un fichier DOT (qui est lui aussi créé). Cette image est celle 
d'une carte représentée à l'aide de Graphviz. Cette option est obtenue
simplement en utilisant l'option `--output-format png` et `--output-filename FILENAME`.
FILENAME sera fait 2 fois, une fois pour le format DOT (avec l'extension ".dot") et une
autre fois pour le format PNG (avec l'extension ".png"). Par exemple, la commande

```sh
$ bin/tp3 --country can --output-format png --output-filename canada.png
```

devrait produire un fichier DOT `canada.dot` ainsi qu'un fichier PNG nommé `canada.png`.

## Affichage de plusieurs pays

Lorsque plus d'un pays est affiché au format "dot" ou "png", il faut également
insérer les liens entres les pays qui partagent une frontière. Par exemple,
supposons que la France, l'Espagne, l'Italie, l'Allemagne et le Portugal forme
une seule région (ce n'est pas le cas dans les données, mais ça fait un exemple
plus court). Alors on obtiendrait un fichier au format "dot" qui ressemblerait
à

```dot
graph {
    fra [
        shape = none,
        label = <<table border="0" cellspacing="0">
                    <tr><td align="center" border="1" fixedsize="true" width="200" height="100"><img src="fra.png" scale="true"/></td></tr>
                    <tr><td align="left" border="1"><b>Name</b>: France</td></tr>
                    <tr><td align="left" border="1"><b>Code</b>: FRA</td></tr>
                </table>>
    ];
    esp [
        shape = none,
        label = <<table border="0" cellspacing="0">
                    <tr><td align="center" border="1" fixedsize="true" width="200" height="100"><img src="esp.png" scale="true"/></td></tr>
                    <tr><td align="left" border="1"><b>Name</b>: Espagne</td></tr>
                    <tr><td align="left" border="1"><b>Code</b>: ESP</td></tr>
                </table>>
    ];
    prt [
        shape = none,
        label = <<table border="0" cellspacing="0">
                    <tr><td align="center" border="1" fixedsize="true" width="200" height="100"><img src="prt.png" scale="true"/></td></tr>
                    <tr><td align="left" border="1"><b>Name</b>: Portugal</td></tr>
                    <tr><td align="left" border="1"><b>Code</b>: PRT</td></tr>
                </table>>
    ];
    ita [
        shape = none,
        label = <<table border="0" cellspacing="0">
                    <tr><td align="center" border="1" fixedsize="true" width="200" height="100"><img src="ita.png" scale="true"/></td></tr>
                    <tr><td align="left" border="1"><b>Name</b>: Italy</td></tr>
                    <tr><td align="left" border="1"><b>Code</b>: ITA</td></tr>
                </table>>
    ];
    deu [
        shape = none,
        label = <<table border="0" cellspacing="0">
                    <tr><td align="center" border="1" fixedsize="true" width="200" height="100"><img src="deu.png" scale="true"/></td></tr>
                    <tr><td align="left" border="1"><b>Name</b>: Germany</td></tr>
                    <tr><td align="left" border="1"><b>Code</b>: GER</td></tr>
                </table>>
    ];

    fra -- esp;
    fra -- deu;
    fra -- ita;
    esp -- prt;


}
```

Et l'image résultante après appel de Graphviz ressemblerait à:

![](images/region.png)

## Plateformes supportées

Le programme a été testé sur Ubuntu 14.04 LTS.

## Dépendances

Ce programme dépend de la bibliothèque non standard `jansson` qu'on peut la récupérer en visitant le site officiel
[jansson](https://jansson.readthedocs.io/en/2.11/gettingstarted.html).
Aussi il faut installer le logiciel `Graphviz` avec la commande suivante sous Ubuntu:
```sh
$  apt-get install Graphviz
```
la base de données coutries.json qu'on peut la récupérer sur le site :
[countries.json] (https://github.com/ablondin/countries).
- [GCC](https://gcc.gnu.org/), pour compiler le projet. C'est le compilateur
  GNU. La version C11 doit être supportée.
- [PKG-config](https://www.freedesktop.org/wiki/Software/pkg-config/), pour
  simplifier la compilation et l'édition des liens avec d'autres bibliothèques.
- [CUnit](http://cunit.sourceforge.net/doc/index.html) pour définir un cadre de
  tests unitaires en C.
- [Graphviz](https://graphviz.gitlab.io/about/), pour générer des images.

## Installation

Commencez par faire un "clone" ou un "fetch" du projet. Ensuite, Il est possible d'installer le programme à partir du
code source en tapant la commande :

```
make
```

qui produit un exécutable nommé `tp3` dans le répertoire `bin`. Il suffit de faire:

```
$ bin/tp3 
```
suivit des options désirées pour l'exécuter à partir du fichier `inf3135-ete2018-tp3/`. 

## Tests

En tout temps, il est possible de vérifier si le programme compile et si son
comportement est cohérent en lançant une suite de tests unitaires, suivit de tests bats. Pour cela,
il suffit d'entrer la commande
```
make test
```

S'assurer d'avoir installé toutes les [dépendances](#dépendances) avant de
lancer les tests automatiques.

## Références

-[parse_args.c](https://gitlab.com/ablondin/inf3135-ete2018-tp2), un module d'un 
travail pratique donné dans le cadre du cours INF3135 à la session d'été
2018, qui traite les arguments par la bibliothèque `getops`.
-[utils.c](https://gitlab.com/ablondin/inf3135-ete2018-tp2), un autre module d'un
travail pratique donné dans le cadre du cours INF3135 à la session d'été
2018, contient quelque fonctions utilent.
-[Makefile](https://gitlab.com/ablondin/inf3135-ete2018-tp2), un autre module d'un
travail pratique donné dans le cadre du cours INF3135 à la session d'été
2018, qui automatise la compilation d'un programme.
-[jansson.c](https://gitlab.com/sim590/inf3135-labo08-ete2018), la solution du numéro 2 des exercices
du laboratoire 8 donné dans le cadre du cours INF3135 à la session d'été 2018 qui lit un fichier de 
format "json".

## Division des tâches


- [X] l'écriture du fichier README.md (Youssef Benkirane)
- [X] création d'une base pour le programme qui supporte la commande `$ bin/tp3` et l'option `--help` (Philippe Lemieux)
- [X] intégration des options `--show-capital` `--show-languages` `--show-borders` (Philippe Lemieux)
- [X] intégration des options `--country COUNTRY` `--region REGION` (Philippe Lemieux & Youssef Benkirane)
- [X] intégration de l'option `--output-format FORMAT` (Philippe Lemieux & Youssef Benkirane)
- [X] intégration de l'option `--output-filename FILENAME` (Philippe Lemieux & Youssef Benkirane)
- [X] intégration de l'option `--show-flag` (Youssef Benkirane)
- [X] intégration de l'option `--output-filename FILENAME` (Philippe Lemieux & Youssef Benkirane)
- [X] make test (Philippe Lemieux & Youssef Benkirane)
- [X] correction des fuites de mémoire. (Philippe Lemieux)
- [X] création de `.gitlab-ci.yml`. (Youssef Benkirane)

## État du projet
le projet est complet.
