/**
 * Main entry point of the program.
 *
 * Basically, it retrieves the arguments provided by the user and launch the
 * simulation, either by printing it to stdout as [...] or [...] .
 * 
 * Based on `tp2.c` from: 
 * https://gitlab.com/ablondin/inf3135-ete2018-tp2/tree/master/src
 *
 * @author Philippe Lemieux
 * @author Youssef Benkirane

 */
#include <stdio.h>
#include "parse_args.h"
#include "json.h"

int main(int argc, char **argv) {
    struct Arguments *arguments = parse_arguments(argc, argv);
    //if (fopen(arguments->namefile,"r")!=NULL) printf("file %s has been opened", arguments->namefile);
    if (arguments->status != TP3_OK) {
        return arguments->status;
    } else {
      //  sleep(100);
        // if (fopen(arguments->namefilepng,"r")!=NULL)  printf("file %s has been created by succes", arguments->namefilepng);
	    print_json(arguments);    
    }
    free_arguments(arguments);
    
    return TP3_OK;
}
