/**
 * Provides basic services to process the main arguments. Error codes are also
 * provided in the case of invalid input from the user.
 * 
 * Based on `parse_args.h` from:
 * https://gitlab.com/ablondin/inf3135-ete2018-tp2/tree/master/src
 *
 * @Youssef Benkirane
 * @author Philippe Lemieux
 */
#ifndef PARSE_ARGS_H
#define PARSE_ARGS_H

#include <stdbool.h>
#include <stdio.h>

#define USAGE "\
Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]\n\
  [--show-languages] [--show-capital] [--show-borders] [--show-flag]\n\
  [--country COUNTRY] [--region REGION]\n\
\n\
Displays information about countries.\n\
\n\
Optional arguments:\n\
 --help                       Show this help message and exit.\n\
 --output-format FORMAT       Selects the ouput format (either \"text\", \"dot\" or \"png\").\n\
                              The \"dot\" format is the one recognized by Graphviz.\n\
                              the default format is \"text\".\n\
 --output-filename FILENAME   The name of the output filename. This argument is\n\
                              mandatory for the \"png\" format. For the \"text\" and \"dot\"\n\
                              format, the result is printed on stdout \n\
                              if no output filename is given.\n\
 --show-languages             The official languages of each country are displayed.\n\
 --show-capital               The capital of each country is displayed.\n\
 --show-borders               The borders of each country are displayed.\n\
 --show-flag                  The flag of each country is displayed\n\
                              (only for \"dot\" and \"png\" format).\n\
 --country COUNTRY            The country code (e.g. \"can\", \"usa\") to be displayed.\n\
 			      Cannot be used with `--region REGION`.\n\
 --region REGION              The region of the countries to be displayed.\n\
                              The supported regions are \"africa\", \"americas\",\n\
                              \"asia\", \"europe\" and \"oceania\".\n\
			      Cannot be used with `--country COUNTRY`.\n\
"

/**
 * The status of the program.
 */
enum Status {
    TP3_OK,                         /**< Everything is alright */
    TP3_COUNTRY_WITH_REGION,        /**< used `--country` with `--region` */
    TP3_ERROR_TOO_MANY_ARGUMENTS,   /**< Too many arguments */
    TP3_FLAG_WITH_TEXT,             /**< Used `--show-flag` with `--output-format text` */
    TP3_BAD_OPTION,                 /**< Bad option */
    TP3_WRONG_NAME,		    /**< Country or region doesn't exist */
    TP3_BAD_EXTENSION,              /**< Extension not supported*/
    TP3_PNG_WITHOUT_FILE	    /**< Used png format without `--output-filename FILENAME */
};

/**
 * The format of output
 */
enum Format {
    TEXT,
    DOT,
    PNG
};

/**
 * The type of search used in `*.json` file
 */
enum Search {
    DEFAULT,
    COUNTRY,
    REGION
};

/**
 * The parsed arguments.
 */
struct Arguments {
    enum Status status;         /**< The status of the parsing */
    enum Search search_by;	    /**< The method used to search `*.json` */
    char* name;			        /**< name of the search country/region */
    enum Format format;		    /**< Format of output */
    FILE* output;               /**< Emplacement of the output */
    char* namefilepng;          /**< the name of the png file */
    char* namefile;             /**< the name of the dot file */
    bool show_languages; 	    /**< To print languages */
    bool show_capital;		    /**< To print capital */
    bool show_borders;		    /**< To print bordesr */
    bool show_flag;		        /**< to print flag */
};

/**
 * Prints how to use the program.
 *
 * @param argv  The arguments provided by the user
 */
void print_usage();

/**
 * Convert string to uppercase
 *
 * @param s  The `char*` to put in uppercase
 * @return   `char *s` in uppercase
 */
char *to_uppercase(const char *s);

/**
 * Convert string to lowercase
 *
 * @param s  The `char*` to put in lowercase
 * @return   `char *s` in lowercase
 * */

char *to_lowercase(const char *s);
/**
 * Returns the parsed arguments provided by the user.
 *
 * @param argc  The number of arguments including the program name
 * @param argv  The arguments provided by the user
 * @return      The parsed arguments
 */
struct Arguments *parse_arguments(int argc, char **argv);

/**
 * Frees the arguments.
 *
 * @param arguments  The arguments to free
 */
void free_arguments(struct Arguments *arguments);

#endif
