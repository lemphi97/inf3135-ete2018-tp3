/**
 * Testing the `json` module with CUnit.
 *
 * @author Alexandre Blondin Masse
 */
#include "json.h"
#include "CUnit/Basic.h"

void test_validate_default() {
    struct Arguments no_search = {0, 0, NULL, 0, NULL, NULL, NULL, false, false, false, false};
    CU_ASSERT_EQUAL(validate_code_region("CAN", &no_search), false);
    CU_ASSERT_EQUAL(validate_code_region("asia", &no_search), false);
}

void test_validate_country() {
    struct Arguments country = {0, 1, NULL, 0, NULL, NULL, NULL, false, false, false, false};
    CU_ASSERT_EQUAL(validate_code_region("CAN", &country), true);
    CU_ASSERT_EQUAL(validate_code_region("can", &country), false);
}

void test_validate_region() {
    struct Arguments region = {0, 2, NULL, 0, NULL, NULL, NULL, false, false, false, false};
    CU_ASSERT_EQUAL(validate_code_region("americas", &region), true);
    CU_ASSERT_EQUAL(validate_code_region("Americas", &region), false);
}

int main() {
    CU_pSuite pSuite = NULL;
    if (CU_initialize_registry() != CUE_SUCCESS )
        return CU_get_error();

    pSuite = CU_add_suite("Testing validation of country & region", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking default search",
                    test_validate_default) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking country search",
                    test_validate_country) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking region search",
                    test_validate_region) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
	
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}
