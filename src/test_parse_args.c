/**
 * Testing the `parse_args` module with CUnit.
 *
 * @author Philippe Lemieux
 */
#include "parse_args.h"
#include "CUnit/Basic.h"

/**
 * test the struct return when unaffected by arguments.
 */
void test_default() { 
    char *argv[] = {"bin/tp3", NULL};
    int argc = 1;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments->search_by, DEFAULT);
    CU_ASSERT_EQUAL(arguments->name, NULL);
    CU_ASSERT_EQUAL(arguments->format, TEXT);
    CU_ASSERT_EQUAL(arguments->output, NULL);
    CU_ASSERT_EQUAL(arguments->namefilepng, NULL);
    CU_ASSERT_EQUAL(arguments->namefile, NULL);
    CU_ASSERT_EQUAL(arguments->show_languages, false);
    CU_ASSERT_EQUAL(arguments->show_capital, false);
    CU_ASSERT_EQUAL(arguments->show_borders, false);
    CU_ASSERT_EQUAL(arguments->show_flag, false);
    free_arguments(arguments);
}

void test_capital() {
    char *argv[] = {"bin/tp3", "--show-capital", NULL};
    int argc = 2;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments->show_capital, true);
    free_arguments(arguments);
}

void test_languages() {
    char *argv[] = {"bin/tp3", "--show-languages", NULL};
    int argc = 2;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments->show_languages, true);
    free_arguments(arguments);
}

void test_borders() {
    char *argv[] = {"bin/tp3", "--show-borders", NULL};
    int argc = 2;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments->show_borders, true);
    free_arguments(arguments);
}

void test_flag() {
    char *argv[] = {"bin/tp3", "--show-flag", "--output-format", "dot", NULL};
    int argc = 4;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments->format, DOT);
    CU_ASSERT_EQUAL(arguments->show_flag, true);
}

void test_country() {
    char *argv[] = {"bin/tp3", "--country", "can", NULL};
    int argc = 3;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments->search_by, COUNTRY);
    CU_ASSERT_EQUAL(strcmp(arguments->name, "CAN"), 0);
    free_arguments(arguments);
}

void test_region() {
    char *argv[] = {"bin/tp3", "--region", "asia", NULL};
    int argc = 3;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments->search_by, REGION);
    CU_ASSERT_EQUAL(strcmp(arguments->name, "Asia"), 0);
    free_arguments(arguments);
}

void test_filename() {
    char *argv1[] = {"bin/tp3", "--output-filename", "test_file.text", NULL};
    int argc = 3;
    struct Arguments *arguments = parse_arguments(argc, argv1);
    CU_ASSERT_EQUAL(arguments->status, TP3_OK);  
    CU_ASSERT_EQUAL(arguments->format, TEXT);
    CU_ASSERT_NOT_EQUAL(arguments->output, NULL); 
    CU_ASSERT_EQUAL(strcmp(arguments->namefile, "test_file.text"), 0);
    free_arguments(arguments);

    char *argv2[] = {"bin/tp3", "--output-filename", "test_file.dot", "--output-format", "dot", NULL};
    argc = 5;
    struct Arguments *arguments1 = parse_arguments(argc, argv2); 
    CU_ASSERT_EQUAL(arguments1->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments1->format, DOT);
    CU_ASSERT_NOT_EQUAL(arguments1->output, NULL);
    CU_ASSERT_EQUAL(strcmp(arguments1->namefile, "test_file.dot"), 0);
    free_arguments(arguments1);
}

void test_format() {
    char *argv1[] = {"bin/tp3", "--output-format", "text", NULL};
    int argc = 3;
    struct Arguments *arguments = parse_arguments(argc, argv1);
    CU_ASSERT_EQUAL(arguments->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments->format, TEXT);

    char *argv2[] = {"bin/tp3", "--output-format", "dot", NULL};
    argc = 3;
    struct Arguments *arguments1 = parse_arguments(argc, argv2);
    CU_ASSERT_EQUAL(arguments1->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments1->format, DOT);
   
    char *argv3[] = {"bin/tp3", "--output-filename", "test_file.png", "--output-format", "png", NULL}; 
    argc = 5;
    struct Arguments *arguments3 = parse_arguments(argc, argv3);
    CU_ASSERT_EQUAL(arguments3->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments3->format, PNG);
    CU_ASSERT_NOT_EQUAL(arguments3->output, NULL);
    CU_ASSERT_EQUAL(strcmp(arguments3->namefilepng, "test_file.png"), 0);
    CU_ASSERT_EQUAL(strcmp(arguments3->namefile, "test_file.dot"), 0);

    
    char *argv4[] = {"bin/tp3", "--output-format", "png", "--output-filename", "test_file.png", NULL};
    argc = 5;
    struct Arguments *arguments4 = parse_arguments(argc, argv4);
    CU_ASSERT_EQUAL(arguments4->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments4->format, PNG);
    CU_ASSERT_NOT_EQUAL(arguments4->output, NULL);
    CU_ASSERT_EQUAL(strcmp(arguments4->namefilepng, "test_file.png"), 0);
    CU_ASSERT_EQUAL(strcmp(arguments4->namefile, "test_file.dot"), 0);

    char *argv5[] = {"bin/tp3", "--output-format", "png", "--output-filename", "test_file.svg", NULL};
    argc = 5;
    struct Arguments *arguments5 = parse_arguments(argc, argv5);
    CU_ASSERT_EQUAL(arguments5->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments5->format, PNG);
    CU_ASSERT_NOT_EQUAL(arguments5->output, NULL);
    CU_ASSERT_EQUAL(strcmp(arguments5->namefilepng, "test_file.png"), 0);
    CU_ASSERT_EQUAL(strcmp(arguments5->namefile, "test_file.dot"), 0);

    char *argv6[] = {"bin/tp3", "--output-format", "png", "--output-filename", "test_file", NULL};
    argc = 5;
    struct Arguments *arguments6 = parse_arguments(argc, argv6);
    CU_ASSERT_EQUAL(arguments6->status, TP3_OK);
    CU_ASSERT_EQUAL(arguments6->format, PNG);
    CU_ASSERT_NOT_EQUAL(arguments6->output, NULL);
    CU_ASSERT_EQUAL(strcmp(arguments6->namefilepng, "test_file.png"), 0);
    CU_ASSERT_EQUAL(strcmp(arguments6->namefile, "test_file.dot"), 0);
}

void test_to_uppercase(){
    CU_ASSERT_EQUAL(strcmp(to_uppercase("can"), "CAN"), 0);
}

void test_to_lowercase(){
    CU_ASSERT_EQUAL(strcmp(to_lowercase("CAN"), "can"), 0);
}

int main() {
    CU_pSuite pSuite = NULL;
    if (CU_initialize_registry() != CUE_SUCCESS )
        return CU_get_error();

    // Testing arguments parsing
    pSuite = CU_add_suite("Testing arguments parsing", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking default arguments",
                    test_default) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking `--show-capital` option",
                    test_capital) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking `--show-languages` option",
                    test_languages) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking `--show-borders` option",
                    test_borders) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking `--show-flag` option",
                    test_flag) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking `--country COUNTRY` option",
                    test_country) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking `--region REGION` option",
                    test_region) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking `--output-filename FILENAME` option",
                    test_filename) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking `--output-format FORMAT` option",
                    test_format) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Testing to_uppercase fonction
    pSuite = CU_add_suite("Testing string to uppercase", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking fonction to_uppercase",
                    test_to_uppercase) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Testing to_lowercase fonction
    pSuite = CU_add_suite("Testing string to lowercase", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking fonction to_lowercase",
                    test_to_lowercase) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}
