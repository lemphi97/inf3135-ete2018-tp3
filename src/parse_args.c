/**
 * Implements parse_args.h, using the getopt library to simplify the
 * processing.
 * 
 * Based on `parse_args.c` from:
 * https://gitlab.com/ablondin/inf3135-ete2018-tp2/tree/master/src
 *
 * @author Philippe Lemieux
 * @author Youssef Benkirane
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <getopt.h>
#include "json.h"
#include "parse_args.h"

#define DELIM '.'
#define DELIMS ","

// ------- //
// Private //
// ------- //

/**
 * Duplicates a string.
 *
 * Taken from `util.c` from:
 * https://gitlab.com/ablondin/inf3135-ete2018-tp2/blob/master/src/utils.c
 *
 * @param s  The string to duplicate
 * @return   A copy of the string
 */
char *strdupli(const char *s) {
    char *t = malloc((strlen(s) + 1) * sizeof(char));
    unsigned int i;
    for (i = 0; s[i] != '\0'; ++i) t[i] = s[i];
    t[i] = '\0';
    return t;
}


/**
 * Retrieves the name of country or region from a string.
 * 
 * @param s          The string from which the type is retrieved
 * @param arguments  The parsed arguments
 * @return           The status of the extraction
 */

enum Status get_name(char *s, struct Arguments *arguments) {
    if(arguments->search_by == COUNTRY) s = to_uppercase(s);
    if(validate_code_region(s, arguments)){
       	if(arguments->search_by == REGION){
	             if(strcmp(s,"africa") == 0) s = "Africa";
                else if(strcmp(s,"americas") == 0) s = "Americas";
                else if(strcmp(s,"asia") == 0) s = "Asia";
                else if(strcmp(s,"europe") == 0) s = "Europe";
                else if(strcmp(s,"oceania") == 0) s = "Oceania";	
	            arguments->name = strdupli(s);
	       }else arguments->name = s;
	return TP3_OK;
    }else{
	return TP3_WRONG_NAME;
    }
}

/**
 * Sets the default arguments.
 *
 * @param arguments  The parsed arguments
 */
void set_default(struct Arguments *arguments) {
    arguments->status = TP3_OK;
    arguments->search_by = DEFAULT;
    arguments->format = TEXT;
    arguments->show_languages = false;
    arguments->show_capital = false;
    arguments->show_borders = false;
    arguments->show_flag = false;
   }

/**
 * Close program after print USAGE and the error message.
 * 
 * @param arguments  The parsed arguments
 */
void exit_program(struct Arguments *arguments){
    if(arguments->status == TP3_COUNTRY_WITH_REGION){
        printf("Error: can't use the options `--country COUNTRY` with `--region REGION`.\n");
    }else if(arguments->status == TP3_FLAG_WITH_TEXT){ 
        printf("Error: can't use text format with `--show-flag`.\n");
    }else if(arguments->status == TP3_WRONG_NAME){
	    printf( "Error: couldn't find country or region.\n");
    }else if(arguments->status == TP3_BAD_EXTENSION){
        printf("Error: only support format png, dot, text.\n");
    }else if(arguments->status == TP3_ERROR_TOO_MANY_ARGUMENTS){
        printf("Error: too many arguments.\n");
    }else if(arguments->status == TP3_PNG_WITHOUT_FILE){
	printf("Error: can't use png format without filename for output.\n");
    }
    print_usage();
    exit(arguments->status);
}

// ------ //
// Public //
// ------ //
void print_usage() {
     printf(USAGE);
    
}

 
/**
 * Convert string to lowercase
 *
 * @param s  The `char*` to put in lowercase
 * @return   `char *s` in lowercase
 */
char *to_lowercase(const char *s){
    char *t = malloc((strlen(s) + 1) * sizeof(char));
    unsigned int i;
    for (i = 0; s[i] != '\0'; ++i) t[i] = tolower((unsigned char)s[i]);
    t[i] = '\0';  
    return t;
}
/**
 * Convert string to uppercase
 *
 * @param s  The `char*` to put in uppercase
 * @return   `char *s` in uppercase
 */
char *to_uppercase(const char *s){
    char *t = malloc((strlen(s) + 1) * sizeof(char));
    unsigned int i;
    for (i = 0; s[i] != '\0'; ++i) t[i] = toupper((unsigned char)s[i]);
    t[i] = '\0';  
    return t;
}

struct Arguments *parse_arguments(int argc, char *argv[]) {
    struct Arguments *arguments = malloc(sizeof(struct Arguments));

    // Temporary variables
    bool show_help = false;
    bool format_text = false;
    bool use_country = false;
    bool use_region = false;
    bool use_flag = false;
    bool use_png = false;
    bool use_output = false;

    set_default(arguments);

    // Resets index
    optind = 0;
    struct option long_opts[] = {
        // Set flag
        {"help",            no_argument,       0, 'h'},
	{"show-flag",       no_argument,       0, 'f'},
	{"country",         required_argument, 0, 'c'},
        {"region",          required_argument, 0, 'r'},
	{"output-format",   required_argument, 0, 'o'},
        // Don't set flag
	{"output-filename", required_argument, 0, 'O'},
	{"show-languages",  no_argument,       0, 'l'},
        {"show-capital",    no_argument,       0, 'C'},
        {"show-borders",    no_argument,       0, 'b'},
        {0, 0, 0, 0}
    };

    // Parse options
    while (true) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "hc:r:o:O:lCbf",
                            long_opts, &option_index);
        if (c == -1) break;
        switch (c) {
            case 'h': show_help = true;
                      break;
	    case 'f': if (arguments->status == TP3_OK){
			  if (format_text){
			      arguments->status = TP3_FLAG_WITH_TEXT;
			      exit_program(arguments);
			  }
			  use_flag = true;
			  arguments->show_flag = true;
		      }
		      break;
 	    case 'c': if (arguments->status == TP3_OK){
			  if (use_region){
			      arguments->status = TP3_COUNTRY_WITH_REGION;
		              exit_program(arguments);
			  }
		      use_country = true;
		      arguments->search_by = COUNTRY;
		      arguments->status = get_name(optarg, arguments);
		      }
		      break;
	    case 'r': if (arguments->status == TP3_OK){
                          if (use_country){
			      arguments->status = TP3_COUNTRY_WITH_REGION;
                              exit_program(arguments);
                          }
                      use_region = true;
                      arguments->search_by = REGION;
                      arguments->status = get_name(optarg, arguments);
                      }
                      break;
	    case 'o': if (arguments->status == TP3_OK){
                   if (strcmp(optarg,"dot") == 0){ 
                        arguments->format = DOT;                        
                    } else if (strcmp(optarg,"png") == 0){
                        arguments->format = PNG;
                        use_png = true;
                    } else if (strcmp(optarg,"text") == 0) {
                                format_text = true;
                    } else arguments->status = TP3_BAD_EXTENSION;
	            if (arguments->format == TEXT){
	                if (use_flag){
	                        arguments->status = TP3_FLAG_WITH_TEXT;
	                        exit_program(arguments);                     
	                }
               	    }
		   }
		    break;
		   
	    case 'O': if (arguments->status == TP3_OK){
			    arguments->namefile = malloc(strlen(optarg)+1 * sizeof(char));
                            strcpy(arguments->namefile, optarg);
			    use_output = true;
                    }
		      break;
	    case 'l': arguments->show_languages = true;
		      break;
	    case 'C': arguments->show_capital = true;
		      break;
	    case 'b': arguments->show_borders = true;
		      break;
            case '?': if (arguments->status == TP3_OK) {
                          arguments->status = TP3_BAD_OPTION;
                      }
        break;
        }
    }

    if(use_png){
	if(use_output){
    	    unsigned int i;
            char *tmp;
            if (strchr(arguments->namefile, '.')){
                tmp = malloc((strlen(arguments->namefile)+4) * sizeof(char));
                for (i = 0; arguments->namefile[i] != '.'; ++i) tmp[i] = arguments->namefile[i];
                tmp[i] = '\0';
                arguments->namefile = realloc(arguments->namefile,(strlen(tmp)) * sizeof(char));
                strcpy(arguments->namefile, tmp);
                strcat(arguments->namefile, ".dot");
                arguments->output = fopen (arguments->namefile, "w");


            } else {
                tmp = malloc(strlen(arguments->namefile) * sizeof(char));
                for (i = 0; arguments->namefile[i] != '\0'; ++i) tmp[i] = arguments->namefile[i];
                tmp[i] = '\0';
                arguments->namefile = realloc(arguments->namefile,(strlen (arguments->namefile) + strlen(".dot")) * sizeof(char));
                strcpy(arguments->namefile, tmp);
                strcat(arguments->namefile,".dot" );
                arguments->output = fopen (arguments->namefile, "w");
            }
            strcat(tmp,".png");
            arguments->namefilepng = malloc(strlen(tmp) *sizeof(char));
            strcpy(arguments->namefilepng, tmp);
            arguments->namefilepng[(strlen(tmp))] = '\0';
            free(tmp);
        } else {
	    if (arguments->output == NULL && arguments->format == PNG) {
	    arguments->status = TP3_PNG_WITHOUT_FILE;
            exit_program(arguments);
	    }
	}
    } else if(use_output) {
	    arguments->output = fopen (arguments->namefile, "w");
    }

    if (arguments->status == TP3_BAD_EXTENSION){
	    exit_program(arguments);
    }
    if (arguments->format == TEXT && use_flag) { 
	    arguments->status = TP3_FLAG_WITH_TEXT;
            exit_program(arguments);
    }
    if (arguments->status == TP3_WRONG_NAME){
	    exit_program(arguments);
    }
    if (optind < argc) {
        arguments->status = TP3_ERROR_TOO_MANY_ARGUMENTS;
	exit_program(arguments);
    } else if (show_help) {
        exit_program(arguments);
    }
    return arguments;
}

void free_arguments(struct Arguments *arguments) {
    if (arguments->search_by == COUNTRY || arguments->search_by == REGION) free(arguments->name);
    if (arguments->namefile != NULL && arguments->output != NULL){ 
	    free(arguments->namefile);
	    if (arguments->format == PNG) free(arguments->namefilepng);
    }
    free(arguments);
}

