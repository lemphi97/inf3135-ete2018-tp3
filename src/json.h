/**
 * Provide services to parse `*.json` files
 *
 * @author: Philippe Lemieux
 * @author: Youssef Benkirane
 */
#ifndef JSON_H
#define JSON_H

#include <stdbool.h>
#include <jansson.h>
#include "parse_args.h"

// ----- //
// Types //
// ----- //

// --------- //
// Functions //
// --------- //

/**
 * Parse `*.json` file and print values that are asked in `arguments`.
 *
 * @param arguments  The parsed arguments
 */
void print_json(struct Arguments *arguments);

/**
 * Validate if `s` exist in `countries.json`.
 *
 * @param s	     The string to validate
 * @param arguments  The parsed arguments
 * @return bool	     True if valid
 */
bool validate_code_region(const char *s, struct Arguments *arguments);

#endif
