/**
 * Implements json.h.
 *
 * @author Youssef benkirane
 * @author Philippe Lemieux
 */
#include "json.h"
#include "parse_args.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#define MAX_COUNTRY 250 
#define MAX_LENGTH_BORDERS 51

/**
 * Meant to be used for dot format.
 * Store countries borders, compares them
 * and returns shared borders.
 * 
 * @param arguments	The parsed arguments
 * @return neighbors    countries who share borders
 */
char *share_borders(struct Arguments *arguments){
    FILE* countries = fopen("countries.json", "r");
    json_t* root = json_loadf(countries, 0, NULL);
    size_t index;
    json_t *value;
    char *marge = "\n    ";
    char *neighbors = malloc((strlen((marge) + 1)) * sizeof(char));
    char country_borders[MAX_COUNTRY][MAX_LENGTH_BORDERS];
    unsigned int x;
    unsigned int y = 0;
	
    strcpy(neighbors, marge);
    json_array_foreach(root, index, value) {
        json_t* region_json = json_object_get(value, "region");
        const char* region = json_string_value(region_json);

        if(arguments->search_by == DEFAULT || (arguments->search_by == REGION && strcmp(arguments->name, region) == 0)){
            json_t* code_json = json_object_get(value, "cca3");
            const char* code = json_string_value(code_json);
	    for(x = 0; x < 3; ++x) {
		country_borders[y][x] = code[x];
	    }
	    json_t* borders_json = json_object_get(value, "borders");
            json_t* border_json = json_array_get(borders_json, 0);
            const char* border = json_string_value(border_json);
	    x = 2;
	    if(border != NULL){
		    int i;
		    for(i = 0; x < 5; ++i) {
			country_borders[y][++x] = border[i];
		    }
	    }
	    border_json = json_array_get(borders_json, 1);
	    x = 5;
            for(int i = 1; border_json != NULL; ++i){
                border_json = json_array_get(borders_json, i);
                const char* border = json_string_value(border_json);
		country_borders[y][++x] = border[0];
		country_borders[y][++x] = border[1];
		country_borders[y][++x] = border[2];
                border_json = json_array_get(borders_json, i+1);
	    }
	    ++y;
	}
    }
    y = 0;
    unsigned int unchecked = 0;

    while(country_borders[unchecked][0] != 0){
	x = 0;
        char *a_code = malloc(4 * sizeof(char));
	char *a_border = malloc(4 * sizeof(char));
	while(x < 3) {
	    a_code[x] = country_borders[unchecked][x];
	    ++x;
	}
	a_code[3] = '\0';
	++unchecked;
	y = unchecked;
	
	while(country_borders[y][0] != 0){
	    while(country_borders[y][x] != 0 && x < 52){
		a_border[0] = country_borders[y][x++];
		a_border[1] = country_borders[y][x++];
	        a_border[2] = country_borders[y][x++];
		a_border[3] = '\0';
	        if (a_code[0] == a_border[0] && a_code[1] == a_border[1] && a_code[2] == a_border[2]){ 
		    char *link = "--";
		    a_border[0] = country_borders[y][0]; 
                    a_border[1] = country_borders[y][1];
               	    a_border[2] = country_borders[y][2];
                    a_border[3] = '\0';
		    neighbors = realloc(neighbors, strlen(neighbors) + (strlen(marge) + strlen(a_border) + strlen(a_code) + strlen(link) + 5) * sizeof(char)); 
		    char *tmp1 = to_lowercase(a_code); 
		    char *tmp2 = to_lowercase(a_border); 
		    strcat(neighbors, tmp1);
		    free(tmp1);
		    strcat(neighbors, link);
		    strcat(neighbors, tmp2);
		    free(tmp2);
		    strcat(neighbors, marge);
	        }
	    }
	    ++y;
	    x = 3;
	}
	free(a_code); free(a_border);
    }
    json_decref(root);
    fclose(countries);
    return neighbors;
}

// ------ //
// Public //
// ------ //

void print_json(struct Arguments *arguments){
    FILE* countries = fopen("countries.json", "r");
    json_t* root = json_loadf(countries, 0, NULL);
    size_t index;
    json_t *value;
    char *bord = "";
    char *lang;
    char *cod;
    char buf[2048];

    if (arguments->format == DOT || arguments->format == PNG) {
                if (arguments->output != NULL) {
                    fprintf(arguments->output,"graph {\n");
                } else  printf("graph {\n");
    }                               
             
   
json_array_foreach(root, index, value) {
		    json_t* code_json = json_object_get(value, "cca3");
            const char* code = json_string_value(code_json);
         
	        json_t* region_json = json_object_get(value, "region");
            const char* region = json_string_value(region_json);

	    if(arguments->search_by == DEFAULT || (arguments->search_by == COUNTRY
		        && strcmp(arguments->name, code) == 0) ||
			                 (arguments->search_by == REGION &&
	  			      strcmp(arguments->name, region) == 0)){		      
            
		         json_t* name_json = json_object_get(value, "name");
                 const char* name = json_string_value(json_object_iter_value(
                 json_object_iter(name_json)));
                if (arguments->output == NULL) {
                     if (arguments->format == TEXT ){
                            printf("Name: %s\nCode: %s\n", name, code);
                     } else {
                        cod = malloc(strlen(code) * sizeof(char));
                        strcpy(cod, code);
                        if (arguments->format == DOT) {
			    char *tmp = to_lowercase(code);
     			    printf("%s [\n    shape = none,\n", tmp);       
			    free(tmp);
            		    printf ("    label = <<table border=\"0\" cellspacing=\"0\">\n");
             		    if (arguments->show_flag) {
                 		system("stty -echo");
                 		system("cd image");
                 		getcwd(buf, 2048 );
               		  	system("cd ..");
                 		system("stty echo");
                		printf("         <tr><td align=\"center\" border=\"1\" fixedsize=\"true\" width=\"200\" height=\"100\">");
				char *tmp = to_lowercase(cod);
                		printf("<img src=\"%s/image/%s\" scale=\"true\"/></td></tr>\n",buf,strcat(tmp,".png"));
				free(tmp);
            		    }
            		    printf("         <tr><td align=\"left\" border=\"1\"><b>Name</b>:%s</td></tr>\n",name);
           		    printf("         <tr><td align=\"left\" border=\"1\"><b>Code</b>: %s</td></tr>\n",code);
           		    if (code != NULL) free(cod);
                        }
                     }
                 } else {  //traitement pour fichier dot
	                if (arguments->format == DOT || arguments->format == PNG) {
                         cod = malloc(strlen(code) * sizeof(char));
                        strcpy(cod, code);
			char *tmp = to_lowercase(code);
                 fprintf(arguments->output, "%s [\n    shape = none,\n", tmp);
	  	 free(tmp);
            fprintf (arguments->output,"    label = <<table border=\"0\" cellspacing=\"0\">\n");
            if (arguments->show_flag) {
                 system("stty -echo");
                 system("cd image");
                 getcwd(buf, 2048 );
                 system("cd ..");
                 system("stty echo");
            fprintf(arguments->output,"         <tr><td align=\"center\" border=\"1\" fixedsize=\"true\" width=\"200\" height=\"100\">");
	    char *tmp1 = to_lowercase(cod);
            fprintf(arguments->output,"<img src=\"%s/image/%s\" scale=\"true\"/></td></tr>\n",buf,strcat(tmp1,".png"));
	    free(tmp1);
             }
            fprintf(arguments->output,"         <tr><td align=\"left\" border=\"1\"><b>Name</b>:%s</td></tr>\n",name);
            fprintf(arguments->output,"         <tr><td align=\"left\" border=\"1\"><b>Code</b>: %s</td></tr>\n",code);
                 if (code != NULL) free(cod);
                  } else fprintf(arguments->output,"Name: %s\nCode: %s\n", name, code);
                 }      
                 	            
        if(arguments->show_capital){
	                    json_t* capitales_json = json_object_get(value, "capital");
                        json_t* capitale_json = json_array_get(capitales_json, 0);
		                const char* capital = json_string_value(capitale_json);
                         if (arguments->output == NULL) {
                             if (arguments->format == TEXT){ 
                                 printf("Capital:");
	                            if(capital != NULL) printf(" %s\n", capital);
                                else printf("\n");
                             } else if (arguments->format == DOT) {
                                printf ("         <tr><td align=\"left\" border=\"1\"><b>Capital</b>:%s </td></tr>\n", capital);
                            }
                        } else {
                         if (arguments->format == DOT || arguments->format == PNG) {                                
             fprintf (arguments->output,"         <tr><td align=\"left\" border=\"1\"><b>Capital</b>:%s </td></tr>\n", capital); 
                        } else if (arguments->format == TEXT){
                                fprintf(arguments->output,"Capital:");
	                            if(capital != NULL) fprintf(arguments->output," %s\n", capital);
                                else fprintf(arguments->output,"\n");
                            }
	                    }
                        
            }

	    if(arguments->show_languages){
		       json_t* languages_json = json_object_get(value, "languages");
               void *iter = json_object_iter(languages_json);
               const char* languages = json_string_value(json_object_iter_value(iter));
               if (arguments->output == NULL) {
                    if (arguments->format == TEXT){ 
                        printf("Languages:");
                        if(languages != NULL) printf(" %s", languages);
                        
                    } else if (arguments->format == DOT && languages != NULL){
                         lang = malloc(strlen(languages) * sizeof(char));
                         strcpy(lang,languages);
                   }
                } else {
                    if (arguments->format == TEXT){
                        fprintf(arguments->output,"Languages:");
                        if(languages != NULL) fprintf(arguments->output," %s", languages);
                    
                    } else if ((arguments->format == DOT || arguments->format == PNG) && languages != NULL){
                        lang = malloc(strlen(languages) * sizeof(char));
                        strcpy(lang,languages);
                    }     
                }
                  iter = json_object_iter_next(languages_json, iter);
                  while(iter){
                    languages = json_string_value(json_object_iter_value(iter));
                    iter = json_object_iter_next(languages_json, iter);
                    if (arguments->output == NULL){
                        if (arguments->format == TEXT) {
                            printf(", %s", languages);
                        } else {
                                lang =  realloc(lang,(strlen(lang) + strlen(languages) * sizeof(char) + 5));
                                strcat(lang,",");
                                strcat(lang,languages);
                        }
                     } else { 
                         if (arguments->format == TEXT) fprintf(arguments->output,", %s", languages);
                         else {
                                lang = realloc(lang,(strlen(lang) + strlen(languages) * sizeof(char) + 5));
                                strcat(lang,",");
                                strcat(lang,languages);         
                         } 
                     }
                  }
                  if (arguments->output == NULL) {
                 if (arguments->format == TEXT){
                      printf("\n");
                 } else if (arguments->format == DOT) {
            printf ("         <tr><td align=\"left\" border=\"1\"><b>Language</b>: %s</td></tr>\n", lang);
	    		if(languages != NULL) free(lang);///
                 }

            } else {
                 if (arguments->format == DOT || arguments->format == PNG) {
        fprintf (arguments->output,"         <tr><td align=\"left\" border=\"1\"><b>Language</b>: %s</td></tr>\n", lang);
              if (languages != NULL) free(lang);
               } else if (arguments->format == TEXT){
                    fprintf(arguments->output,"\n");
                }
             }
        }
        if(arguments->show_borders){
		    json_t* borders_json = json_object_get(value, "borders");
                    json_t* border_json = json_array_get(borders_json, 0);
                    const char* border = json_string_value(border_json);
                    if (arguments->output == NULL){
                        if (arguments->format == TEXT) {
		                    printf("Borders:");
		                    if(border != NULL) printf(" %s", border);
                            } else if (arguments->format == DOT && border != NULL){
                           bord = malloc(strlen(border) * sizeof(char));
                           strcpy(bord,border);
                         }
                    } else {
                        if (arguments->format == TEXT) {
                            fprintf(arguments->output,"Borders:");
		                    if(border != NULL) fprintf(arguments->output," %s", border);
                            } else if ((arguments->format == DOT || arguments->format == PNG) && border != NULL){
                            bord = malloc(strlen(border) * sizeof(char));
                            strcpy(bord,border);
                        } 
                    }

		     border_json = json_array_get(borders_json, 1);
		     for(int i = 1; border_json != NULL; ++i){
		        border_json = json_array_get(borders_json, i);
                const char* border = json_string_value(border_json);
                if (arguments->output == NULL){
                    if (arguments->format == TEXT && border != NULL)  { 
                        printf(", %s", border);
                    } else if (border != NULL){
                          bord = realloc(bord,(strlen(bord) + strlen(border) * sizeof(char) + 5));
                          strcat(bord,",");
                          strcat(bord,border);                    
                    }

                } else {
                        if (arguments->format == TEXT && border != NULL) {
                             fprintf(arguments->output,", %s", border);
                        } else if (border != NULL){
                          bord = realloc(bord,(strlen(bord) + strlen(border) * sizeof(char) + 5));
                          strcat(bord,",");
                          strcat(bord,border);
                        }
                }

                border_json = json_array_get(borders_json, i+1);
		    }
        if (arguments->output == NULL) {
                if (arguments->format == TEXT) {
                    printf("\n");
                 } else if (arguments->format == DOT) {
                    printf("         <tr><td align=\"left\" border=\"1\"><b>Borders</b>: %s</td></tr>\n",bord);
                 }
        } else {
             if (arguments->format == TEXT) {
                    fprintf(arguments->output,"\n");		    	

             } else if (arguments->format == DOT || arguments->format == PNG) {
        fprintf(arguments->output,"         <tr><td align=\"left\" border=\"1\"><b>Borders</b>: %s</td></tr>\n",bord);
             }
        }
        if ((border != NULL) && (arguments->format == DOT || arguments->format == PNG))  free(bord);
                         
     }               
        if (arguments->format == DOT || arguments->format == PNG) {
                if (arguments->output != NULL) {
                    fprintf(arguments->output,"    </table>>\n   ];\n");
                } else printf("    </table>>\n   ];\n");
        }       
    }
}
 if ((arguments->search_by == REGION || arguments->search_by == DEFAULT) &&  (arguments->format == DOT || arguments->format == PNG)) {
	char *tmp = share_borders(arguments);
        if (arguments->output != NULL) {
            fprintf(arguments->output,"%s", tmp);
        } else {
	    printf("%s", tmp);
 	} 
	free(tmp);
 }
if (arguments->format == DOT || arguments->format == PNG) {
    if (arguments->output != NULL) {
        fprintf(arguments->output,"}\n");
    } else printf("}\n");
} 
      if (arguments->output != NULL && arguments->format == PNG) {
        fclose (arguments->output);        
        execl("/usr/bin/neato","/usr/bin/neato", "-Goverlap=false", "-Tpng", "-o", arguments->namefilepng, arguments->namefile,(char*)NULL);
      } 
        if (fopen(arguments->namefilepng,"r")!=NULL)  printf("file %s has been created by succes", arguments->namefilepng);
       if (arguments->output != NULL) fclose(arguments->output);
     json_decref(root);
     fclose(countries);
     
}

bool validate_code_region(const char *s, struct Arguments *arguments){
    bool valid = false;
     if(arguments->search_by == REGION){
                if(strcmp(s,"africa") == 0) valid = true;
     else if(strcmp(s,"americas") == 0) valid = true;
        else if(strcmp(s,"asia") == 0) valid = true;
        else if(strcmp(s,"europe") == 0) valid = true;
        else if(strcmp(s,"oceania") == 0) valid = true;
    } else if(arguments->search_by == COUNTRY){
        FILE* countries = fopen("countries.json", "r");
        json_t* root = json_loadf(countries, 0, NULL);
        size_t index;
        json_t *value;
        json_array_foreach(root, index, value){
            json_t* code_json = json_object_get(value, "cca3");
            const char* code = json_string_value(code_json);
            if (strcmp(s,code) == 0){
                valid = true;
            }
        }
        json_decref(root);
        fclose(countries);
      }
    return valid;
}
